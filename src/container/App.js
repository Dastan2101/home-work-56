import React, { Component } from 'react';
import './App.css';
import Case from '../components/case';

const arrayCase = [];

const caseFun = () => {


    let random = Math.floor(Math.random() * 36 + 1);

    for (let i = 1; i < 37; i++) {

        const object = {
            hasItem: false
        };

        arrayCase.push(object);

        if (random === i) {
            object.hasItem = true;
        }
    }
    return arrayCase;
};

caseFun();

class App extends Component {
    state = {
        array: arrayCase,
        block: 'none',
    };


    findRing = (index) => {
        if(index.hasItem) {
            this.setState({
                block: 'block'
            })
        }
    };

  render() {
    return (
      <div className="App">
          <div className="case-board" style={{width: '160px', margin: '50px auto'}}>
              <Case block={this.state.block} array={this.state.array} findRing={this.findRing} />
          </div>
      </div>
    );
  }
}

export default App;
