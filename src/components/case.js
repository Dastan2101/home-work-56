import React from 'react';
import './caseStyle.css';

const Case = (props) => {
    return (
            props.array.map((index, key) => {
                return (
                    <div key={key} style={{
                        width: '20px',
                        height: '20px',
                        display: 'inline-block',
                        border: '1px solid black',
                        float: 'left',
                        margin: '2px',
                        background: 'grey'
                    }} onClick={() => (props.findRing(index))} >
                        {index.hasItem ? <span className='ring' style={{display: props.block}} /> : null}
                    </div>
                )
            })
    )
};

export default Case;